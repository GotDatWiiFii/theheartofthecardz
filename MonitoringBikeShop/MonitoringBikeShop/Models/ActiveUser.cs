//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MonitoringBikeShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    /// <summary>
    /// An ActiveUser who is viewing the BikeShop website
    /// </summary>
    public partial class ActiveUser
    {
        /// <summary>
        /// The Primary key
        /// </summary>
        [Key]
        public int activeUser_id { get; set; }
        /// <summary>
        /// The response time for the ActiveUser
        /// </summary>
        public Nullable<System.TimeSpan> responseTime { get; set; }
        /// <summary>
        /// The country the ActiveUser is in
        /// </summary>
        public string country { get; set; }
        /// <summary>
        /// The state or province the ActiveUser is in
        /// </summary>
        public string stateOrProvince { get; set; }
        /// <summary>
        /// Foreign key into the Traffic table
        /// </summary>
        public Nullable<int> traffic_id { get; set; }

        /// <summary>
        /// The Traffic object related to the ActiveUser
        /// </summary>
        public virtual Traffic Traffic { get; set; }
    }
}
