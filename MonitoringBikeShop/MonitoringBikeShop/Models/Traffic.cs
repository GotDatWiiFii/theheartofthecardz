//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MonitoringBikeShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// A report on the Traffic for a BikeShop web site
    /// </summary>
    public partial class Traffic
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Traffic()
        {
            this.ActiveUsers = new HashSet<ActiveUser>();
            this.DowntimeReports = new HashSet<DowntimeReport>();
        }
    
        /// <summary>
        /// The Primary key
        /// </summary>
        [Key]
        public int traffic_id { get; set; }
        /// <summary>
        /// The date the report was made
        /// </summary>
        public Nullable<System.DateTime> date { get; set; }
        /// <summary>
        /// The time the report was made
        /// </summary>
        public Nullable<System.TimeSpan> time { get; set; }
        /// <summary>
        /// Foreign key into the BikeShop table
        /// </summary>
        public Nullable<int> bikeshop_id { get; set; }
    
        /// <summary>
        /// A collection of ActiveUsers related to the Traffic object
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ActiveUser> ActiveUsers { get; set; }
        /// <summary>
        /// The BikeShop object related to the Traffic object
        /// </summary>
        public virtual BikeShop BikeShop { get; set; }
        /// <summary>
        /// A collection of DowntimeReports related to the Traffic object
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DowntimeReport> DowntimeReports { get; set; }
    }
}
