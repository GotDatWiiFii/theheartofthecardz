﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MonitoringBikeShop.Models;

namespace MonitoringBikeShop.Controllers
{
    /// <summary>
    /// Controller to manipulate DowntimeReport objects in the database
    /// </summary>
    public class DowntimeReportsController : ApiController
    {
        private MonitoringDatabaseEntities1 db = new MonitoringDatabaseEntities1();

        // GET: api/DowntimeReports
        /// <summary>
        /// Get all DowntimeReport objects from database
        /// </summary>
        /// <returns>A list of all DowntimeReport objects in the database</returns>
        public IQueryable<DowntimeReport> GetDowntimeReports()
        {
            return db.DowntimeReports;
        }

        // GET: api/DowntimeReports/5
        /// <summary>
        /// Gets the corresponding DowntimeReport object from the database
        /// </summary>
        /// <param name="id">The dowtimeReport_id</param>
        /// <returns>The corresponding DowntimeReport object</returns>
        [ResponseType(typeof(DowntimeReport))]
        public IHttpActionResult GetDowntimeReport(int id)
        {
            DowntimeReport downtimeReport = db.DowntimeReports.Find(id);
            if (downtimeReport == null)
            {
                return NotFound();
            }

            return Ok(downtimeReport);
        }

        // PUT: api/DowntimeReports/5
        /// <summary>
        /// Updates the corresponding DowntimeReport object in the database
        /// </summary>
        /// <param name="id">The downtimeReport_id</param>
        /// <param name="downtimeReport">The new DowntimeReport object</param>
        /// <returns>Message for update or errors</returns>
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDowntimeReport(int id, DowntimeReport downtimeReport)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != downtimeReport.dowtimeReport_id)
            {
                return BadRequest();
            }

            db.Entry(downtimeReport).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DowntimeReportExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DowntimeReports
        /// <summary>
        /// Creates a new DowntimeReport object in the database
        /// </summary>
        /// <param name="downtimeReport">The new DowntimeReport object</param>
        /// <returns>The created DowntimeReport object</returns>
        [ResponseType(typeof(DowntimeReport))]
        public IHttpActionResult PostDowntimeReport(DowntimeReport downtimeReport)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DowntimeReports.Add(downtimeReport);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DowntimeReportExists(downtimeReport.dowtimeReport_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = downtimeReport.dowtimeReport_id }, downtimeReport);
        }

        // DELETE: api/DowntimeReports/5
        /// <summary>
        /// Deletes the corresponding DowntimeReport object from the database
        /// </summary>
        /// <param name="id">The downtimeReport_id</param>
        /// <returns>The deleted DowntimeReport object</returns>
        [ResponseType(typeof(DowntimeReport))]
        public IHttpActionResult DeleteDowntimeReport(int id)
        {
            DowntimeReport downtimeReport = db.DowntimeReports.Find(id);
            if (downtimeReport == null)
            {
                return NotFound();
            }

            db.DowntimeReports.Remove(downtimeReport);
            db.SaveChanges();

            return Ok(downtimeReport);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DowntimeReportExists(int id)
        {
            return db.DowntimeReports.Count(e => e.dowtimeReport_id == id) > 0;
        }
    }
}