﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MonitoringBikeShop.Models;

namespace MonitoringBikeShop.Controllers
{
    /// <summary>
    /// Controller to manipute BikeShop objects in the database
    /// </summary>
    public class BikeShopsController : ApiController
    {
        private MonitoringDatabaseEntities1 db = new MonitoringDatabaseEntities1();
        public BikeShopsController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/BikeShops
        /// <summary>
        /// Gets all BikeShop objects in the database
        /// </summary>
        /// <returns>A list of all BikeShop objects</returns>
        public IQueryable<BikeShop> GetBikeShops()
        {
            return db.BikeShops;
        }

        // GET: api/BikeShops/5
        /// <summary>
        /// Gets a corresponding BikeShop object with id
        /// </summary>
        /// <param name="id">The bikeshop_id</param>
        /// <returns>The corresponding BikeShop object</returns>
        [ResponseType(typeof(BikeShop))]
        public IHttpActionResult GetBikeShop(int id)
        {
            BikeShop bikeShop = db.BikeShops.Find(id);
            if (bikeShop == null)
            {
                return NotFound();
            }

            return Ok(bikeShop);
        }

        // PUT: api/BikeShops/5
        /// <summary>
        /// Updates the corresponding BikeShop object
        /// </summary>
        /// <param name="id">The bikeshop_id</param>
        /// <param name="bikeShop">The new BikeShop object</param>
        /// <returns>Message for update or errors</returns>
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBikeShop(int id, BikeShop bikeShop)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bikeShop.bikeshop_id)
            {
                return BadRequest();
            }

            db.Entry(bikeShop).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BikeShopExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BikeShops
        /// <summary>
        /// Creates a new BikeShop object in the database
        /// </summary>
        /// <param name="bikeShop">The new BikeShop object</param>
        /// <returns>The created BikeShop objects</returns>
        [ResponseType(typeof(BikeShop))]
        public IHttpActionResult PostBikeShop(BikeShop bikeShop)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BikeShops.Add(bikeShop);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (BikeShopExists(bikeShop.bikeshop_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = bikeShop.bikeshop_id }, bikeShop);
        }

        // DELETE: api/BikeShops/5
        /// <summary>
        /// Deletes the corresponding BikeShop object from the database
        /// </summary>
        /// <param name="id">The bikeshop_id</param>
        /// <returns>The deleted BikeShop object</returns>
        [ResponseType(typeof(BikeShop))]
        public IHttpActionResult DeleteBikeShop(int id)
        {
            BikeShop bikeShop = db.BikeShops.Find(id);
            if (bikeShop == null)
            {
                return NotFound();
            }

            db.BikeShops.Remove(bikeShop);
            db.SaveChanges();

            return Ok(bikeShop);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BikeShopExists(int id)
        {
            return db.BikeShops.Count(e => e.bikeshop_id == id) > 0;
        }
    }
}