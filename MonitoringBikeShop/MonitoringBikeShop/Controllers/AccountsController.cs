﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MonitoringBikeShop.Models;

namespace MonitoringBikeShop.Controllers
{
    /// <summary>
    /// Controller to manipulate the Account Table
    /// </summary>
    public class AccountsController : ApiController
    {
        private MonitoringDatabaseEntities1 db = new MonitoringDatabaseEntities1();

        // GET: api/Accounts
        /// <summary>
        /// Gets all accounts in the database
        /// </summary>
        /// <returns>All accounts in the database</returns>
        public IQueryable<Account> GetAccounts()
        {
            return db.Accounts;
        }

        // GET: api/Accounts/5
        /// <summary>
        /// Gets the account from the database with the corresponding id
        /// </summary>
        /// <param name="id">The account_id to retrieve</param>
        /// <returns>The account with the corresponding account_id</returns>
        [ResponseType(typeof(Account))]
        public IHttpActionResult GetAccount(int id)
        {
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return NotFound();
            }

            return Ok(account);
        }

        // PUT: api/Accounts/5
        /// <summary>
        /// Updates the corresponding account
        /// </summary>
        /// <param name="id">The account_id parameter</param>
        /// <param name="account">The account object with the parameters to change to</param>
        /// <returns>Corresponding message upon update or error</returns>
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAccount(int id, Account account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != account.account_id)
            {
                return BadRequest();
            }

            db.Entry(account).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Accounts
        /// <summary>
        /// Creates a new account object to add to the database
        /// </summary>
        /// <param name="account">The account object to add</param>
        /// <returns>The account object created</returns>
        [ResponseType(typeof(Account))]
        public IHttpActionResult PostAccount(Account account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Accounts.Add(account);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (AccountExists(account.account_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = account.account_id }, account);
        }

        // DELETE: api/Accounts/5
        /// <summary>
        /// Deletes the specified account object
        /// </summary>
        /// <param name="id">The account_id to delete</param>
        /// <returns>The account object deleted</returns>
        [ResponseType(typeof(Account))]
        public IHttpActionResult DeleteAccount(int id)
        {
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return NotFound();
            }

            db.Accounts.Remove(account);
            db.SaveChanges();

            return Ok(account);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AccountExists(int id)
        {
            return db.Accounts.Count(e => e.account_id == id) > 0;
        }
    }
}