﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MonitoringBikeShop.Models;

namespace MonitoringBikeShop.Controllers
{
    /// <summary>
    /// Controller to manipulate ActiveUser objects in database
    /// </summary>
    public class ActiveUsersController : ApiController
    {
        private MonitoringDatabaseEntities1 db = new MonitoringDatabaseEntities1();

        // GET: api/ActiveUsers
        /// <summary>
        /// Gets all ActiveUser objects in database
        /// </summary>
        /// <returns>A list of all active users in database</returns>
        public IQueryable<ActiveUser> GetActiveUsers()
        {
            return db.ActiveUsers;
        }

        // GET: api/ActiveUsers/5
        /// <summary>
        /// Gets a single ActiveUser from database with corresponding id
        /// </summary>
        /// <param name="id">The activeUser_id</param>
        /// <returns>The corresponding ActiveUser object</returns>
        [ResponseType(typeof(ActiveUser))]
        public IHttpActionResult GetActiveUser(int id)
        {
            ActiveUser activeUser = db.ActiveUsers.Find(id);
            if (activeUser == null)
            {
                return NotFound();
            }

            return Ok(activeUser);
        }

        // PUT: api/ActiveUsers/5
        /// <summary>
        /// Updates the corresponding ActiveUser with id
        /// </summary>
        /// <param name="id">The activeUser_id</param>
        /// <param name="activeUser">The new ActiveUser object</param>
        /// <returns>Messages corresponding to update or error</returns>
        [ResponseType(typeof(void))]
        public IHttpActionResult PutActiveUser(int id, ActiveUser activeUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != activeUser.activeUser_id)
            {
                return BadRequest();
            }

            db.Entry(activeUser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActiveUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ActiveUsers
        /// <summary>
        /// Creates a new ActiveUser in the database
        /// </summary>
        /// <param name="activeUser">The ActiveUser object to add to the database</param>
        /// <returns>The created ActiveUser object</returns>
        [ResponseType(typeof(ActiveUser))]
        public IHttpActionResult PostActiveUser(ActiveUser activeUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ActiveUsers.Add(activeUser);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ActiveUserExists(activeUser.activeUser_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = activeUser.activeUser_id }, activeUser);
        }

        // DELETE: api/ActiveUsers/5
        /// <summary>
        /// Deletes the corresponding ActiveUser from the database
        /// </summary>
        /// <param name="id">The activeUser_id</param>
        /// <returns>The deleted ActiveUser</returns>
        [ResponseType(typeof(ActiveUser))]
        public IHttpActionResult DeleteActiveUser(int id)
        {
            ActiveUser activeUser = db.ActiveUsers.Find(id);
            if (activeUser == null)
            {
                return NotFound();
            }

            db.ActiveUsers.Remove(activeUser);
            db.SaveChanges();

            return Ok(activeUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ActiveUserExists(int id)
        {
            return db.ActiveUsers.Count(e => e.activeUser_id == id) > 0;
        }
    }
}