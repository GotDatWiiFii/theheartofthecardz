﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MonitoringBikeShop.Models;

namespace MonitoringBikeShop.Controllers
{
    /// <summary>
    /// Controller to manipulate the Traffic table in the database
    /// </summary>
    public class TrafficController : ApiController
    {
        private MonitoringDatabaseEntities1 db = new MonitoringDatabaseEntities1();

        // GET: api/Traffic
        /// <summary>
        /// Gets all Traffic objects from the database
        /// </summary>
        /// <returns>A list of all Traffic objects in the database</returns>
        public IQueryable<Traffic> GetTraffic()
        {
            return db.Traffic;
        }

        // GET: api/Traffic/5
        /// <summary>
        /// Gets the corresponding Traffic object from the database
        /// </summary>
        /// <param name="id">The traffic_id</param>
        /// <returns>The corresponding Traffic object</returns>
        [ResponseType(typeof(Traffic))]
        public IHttpActionResult GetTraffic(int id)
        {
            Traffic traffic = db.Traffic.Find(id);
            if (traffic == null)
            {
                return NotFound();
            }

            return Ok(traffic);
        }

        // PUT: api/Traffic/5
        /// <summary>
        /// Updates the corresponding Traffic object in the database
        /// </summary>
        /// <param name="id">The traffic_id</param>
        /// <param name="traffic">The new Traffic object</param>
        /// <returns>Message for update or errors</returns>
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTraffic(int id, Traffic traffic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != traffic.traffic_id)
            {
                return BadRequest();
            }

            db.Entry(traffic).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrafficExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Traffic
        /// <summary>
        /// Creates a new Traffic object in the database
        /// </summary>
        /// <param name="traffic">The new Traffic object</param>
        /// <returns>The created Traffic object</returns>
        [ResponseType(typeof(Traffic))]
        public IHttpActionResult PostTraffic(Traffic traffic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Traffic.Add(traffic);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TrafficExists(traffic.traffic_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = traffic.traffic_id }, traffic);
        }

        // DELETE: api/Traffic/5
        /// <summary>
        /// Deletes the corresponding Traffic object
        /// </summary>
        /// <param name="id">The traffic_id</param>
        /// <returns>The deleted Traffic object</returns>
        [ResponseType(typeof(Traffic))]
        public IHttpActionResult DeleteTraffic(int id)
        {
            Traffic traffic = db.Traffic.Find(id);
            if (traffic == null)
            {
                return NotFound();
            }

            db.Traffic.Remove(traffic);
            db.SaveChanges();

            return Ok(traffic);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TrafficExists(int id)
        {
            return db.Traffic.Count(e => e.traffic_id == id) > 0;
        }
    }
}