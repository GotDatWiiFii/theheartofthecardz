﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MonitoringBikeShop.Models;

namespace MonitoringBikeShop.Controllers
{
    /// <summary>
    /// Controller to manipulate the Item table in the database
    /// </summary>
    public class ItemsController : ApiController
    {
        private MonitoringDatabaseEntities1 db = new MonitoringDatabaseEntities1();

        // GET: api/Items
        /// <summary>
        /// Gets all items from the database
        /// </summary>
        /// <returns>A list of all items in database</returns>
        public IQueryable<Item> GetItems()
        {
            return db.Items;
        }

        // GET: api/Items/5
        /// <summary>
        /// Gets the corresponding item from the database
        /// </summary>
        /// <param name="id">The item_id</param>
        /// <returns>The corresponding item from the database</returns>
        [ResponseType(typeof(Item))]
        public IHttpActionResult GetItem(int id)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // PUT: api/Items/5
        /// <summary>
        /// Update the corresponding item in the database
        /// </summary>
        /// <param name="id">The item_id</param>
        /// <param name="item">The new item object</param>
        /// <returns>Message for update or errors</returns>
        [ResponseType(typeof(void))]
        public IHttpActionResult PutItem(int id, Item item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.item_id)
            {
                return BadRequest();
            }

            db.Entry(item).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Items
        /// <summary>
        /// Creates a new Item object in the database
        /// </summary>
        /// <param name="item">The new Item object</param>
        /// <returns>The created Item object</returns>
        [ResponseType(typeof(Item))]
        public IHttpActionResult PostItem(Item item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Items.Add(item);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ItemExists(item.item_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = item.item_id }, item);
        }

        // DELETE: api/Items/5
        /// <summary>
        /// Deletes the corresponding Item object
        /// </summary>
        /// <param name="id">The item_id</param>
        /// <returns>The deleted Item object</returns>
        [ResponseType(typeof(Item))]
        public IHttpActionResult DeleteItem(int id)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            db.Items.Remove(item);
            db.SaveChanges();

            return Ok(item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ItemExists(int id)
        {
            return db.Items.Count(e => e.item_id == id) > 0;
        }
    }
}