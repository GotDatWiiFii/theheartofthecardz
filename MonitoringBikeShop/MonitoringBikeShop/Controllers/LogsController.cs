﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MonitoringBikeShop.Models;

namespace MonitoringBikeShop.Controllers
{
    /// <summary>
    /// Controller to manipulate Log objects in the database
    /// </summary>
    public class LogsController : ApiController
    {
        private MonitoringDatabaseEntities1 db = new MonitoringDatabaseEntities1();

        // GET: api/Logs
        /// <summary>
        /// Gets all Log objects from the database
        /// </summary>
        /// <returns>A list of all Log objects from database</returns>
        public IQueryable<Log> GetLogs()
        {
            return db.Logs;
        }

        // GET: api/Logs/5
        /// <summary>
        /// Get the corresponding Log object from the database
        /// </summary>
        /// <param name="id">The log_id</param>
        /// <returns>The corresponding Log object</returns>
        [ResponseType(typeof(Log))]
        public IHttpActionResult GetLog(int id)
        {
            Log log = db.Logs.Find(id);
            if (log == null)
            {
                return NotFound();
            }

            return Ok(log);
        }

        // PUT: api/Logs/5
        /// <summary>
        /// Updates the corresponding Log object
        /// </summary>
        /// <param name="id">The log_id</param>
        /// <param name="log">The new Log object</param>
        /// <returns>Message for update or errors</returns>
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLog(int id, Log log)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != log.log_id)
            {
                return BadRequest();
            }

            db.Entry(log).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Logs
        /// <summary>
        /// Creates a new Log object in the database
        /// </summary>
        /// <param name="log">The new Log object</param>
        /// <returns>The created Log object</returns>
        [ResponseType(typeof(Log))]
        public IHttpActionResult PostLog(Log log)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Logs.Add(log);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (LogExists(log.log_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = log.log_id }, log);
        }

        // DELETE: api/Logs/5
        /// <summary>
        /// Deletes the corresponding Log object from the database
        /// </summary>
        /// <param name="id">The log_id</param>
        /// <returns>The deleted Log object</returns>
        [ResponseType(typeof(Log))]
        public IHttpActionResult DeleteLog(int id)
        {
            Log log = db.Logs.Find(id);
            if (log == null)
            {
                return NotFound();
            }

            db.Logs.Remove(log);
            db.SaveChanges();

            return Ok(log);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LogExists(int id)
        {
            return db.Logs.Count(e => e.log_id == id) > 0;
        }
    }
}