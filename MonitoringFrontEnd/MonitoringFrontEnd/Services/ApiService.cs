﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using MonitoringFrontEnd.Models;

namespace MonitoringFrontEnd.Services
{
    /// <summary>
    /// The service to communicate with the api.
    /// Code adapted from https://carldesouza.com/httpclient-getasync-postasync-sendasync-c/
    /// </summary>
    public class ApiService
    {
        public ApiService(HttpClient client)
        {
            
        }

        /// <summary>
        /// Gets all the entity objects using an API call
        /// </summary>
        /// <param name="u"> The URI associated with the api call</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> GetUriAsync(Uri u)
        {
            HttpResponseMessage response = null;
            using(var _httpClient = new HttpClient())
            {
                response = await _httpClient.GetAsync(u);
            }

            return response;
        }

        /// <summary>
        /// Posts a new entity using an API call
        /// </summary>
        /// <param name="u"> The URI associated with the api call</param>
        /// <param name="content"> The content for the request message</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PostUriAsync(Uri u, HttpContent content)
        {
            HttpResponseMessage response = null;

            //create request
            using(var _httpClient = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = u,
                    Content = content
                };

                //send request
                response = await _httpClient.SendAsync(request);
            }

            return response;
        }


        /// <summary>
        /// Updates an entity using an API call
        /// </summary>
        /// <param name="u"> The URI associated with the api call</param>
        /// <param name="content"> The content for the request message</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PutUriAsync(Uri u, HttpContent content)
        {
            HttpResponseMessage response = null;

            //create request
            using (var _httpClient = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage
                {
                    Method = HttpMethod.Put,
                    RequestUri = u,
                    Content = content
                };

                //send request
                response = await _httpClient.SendAsync(request);
            }

            return response;
        }


        /// <summary>
        /// Deletes an entity using an API Delete call 
        /// </summary>
        /// <param name="u">The URI associated with the api call</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> DeleteUriAsync(Uri u)
        {
            HttpResponseMessage response = null;
            using (var _httpClient = new HttpClient())
            {
                response = await _httpClient.DeleteAsync(u);
            }

            return response;
        }

        
    }
}
