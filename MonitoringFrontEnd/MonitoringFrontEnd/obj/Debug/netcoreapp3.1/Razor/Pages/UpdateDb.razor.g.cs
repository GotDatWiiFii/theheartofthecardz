#pragma checksum "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0f99ab329c744b46876d519727d6530502964744"
// <auto-generated/>
#pragma warning disable 1591
namespace MonitoringFrontEnd.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using MonitoringFrontEnd;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using MonitoringFrontEnd.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor"
           [Authorize(Roles = "ADMIN")]

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/updateDb")]
    public partial class UpdateDb : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3>Update Database</h3>\r\n\r\n");
            __builder.AddMarkupContent(1, "<p>Click on the data entity you wish to update in the database.</p>\r\n\r\n");
            __builder.OpenElement(2, "div");
            __builder.AddAttribute(3, "class", "redirectContainer");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "redirectTable");
            __builder.OpenElement(6, "button");
            __builder.AddAttribute(7, "type", "button");
            __builder.AddAttribute(8, "class", "redirectButton");
            __builder.AddAttribute(9, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 13 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor"
                                                               AccountRedirect

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(10, "Update Account");
            __builder.CloseElement();
            __builder.AddMarkupContent(11, "\r\n        <br>\r\n        ");
            __builder.OpenElement(12, "button");
            __builder.AddAttribute(13, "type", "button");
            __builder.AddAttribute(14, "class", "redirectButton");
            __builder.AddAttribute(15, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 15 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor"
                                                               ActiveUserRedirect

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(16, "Update Active User Report");
            __builder.CloseElement();
            __builder.AddMarkupContent(17, "\r\n        <br>\r\n        ");
            __builder.OpenElement(18, "button");
            __builder.AddAttribute(19, "type", "button");
            __builder.AddAttribute(20, "class", "redirectButton");
            __builder.AddAttribute(21, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 17 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor"
                                                               BikeShopRedirect

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(22, "Update Bike Shop");
            __builder.CloseElement();
            __builder.AddMarkupContent(23, "\r\n        <br>\r\n        ");
            __builder.OpenElement(24, "button");
            __builder.AddAttribute(25, "type", "button");
            __builder.AddAttribute(26, "class", "redirectButton");
            __builder.AddAttribute(27, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 19 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor"
                                                               DowntimeReportRedirect

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(28, "Update Downtime Report");
            __builder.CloseElement();
            __builder.AddMarkupContent(29, "\r\n        <br>\r\n        ");
            __builder.OpenElement(30, "button");
            __builder.AddAttribute(31, "type", "button");
            __builder.AddAttribute(32, "class", "redirectButton");
            __builder.AddAttribute(33, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 21 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor"
                                                               ItemRedirect

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(34, "Update Item");
            __builder.CloseElement();
            __builder.AddMarkupContent(35, "\r\n        <br>\r\n        ");
            __builder.OpenElement(36, "button");
            __builder.AddAttribute(37, "type", "button");
            __builder.AddAttribute(38, "class", "redirectButton");
            __builder.AddAttribute(39, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 23 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor"
                                                               LogRedirect

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(40, "Update Log");
            __builder.CloseElement();
            __builder.AddMarkupContent(41, "\r\n        <br>\r\n        ");
            __builder.OpenElement(42, "button");
            __builder.AddAttribute(43, "type", "button");
            __builder.AddAttribute(44, "class", "redirectButton");
            __builder.AddAttribute(45, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 25 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor"
                                                               TrafficRedirect

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(46, "Update Traffic Report");
            __builder.CloseElement();
            __builder.AddMarkupContent(47, "\r\n        <br>\r\n        ");
            __builder.OpenElement(48, "button");
            __builder.AddAttribute(49, "type", "button");
            __builder.AddAttribute(50, "class", "redirectButton");
            __builder.AddAttribute(51, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 27 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor"
                                                               TransactionRedirect

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(52, "Update Transaction Report");
            __builder.CloseElement();
            __builder.AddMarkupContent(53, "\r\n        <br>");
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 33 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDb.razor"
       
    void AccountRedirect() { uriHelper.NavigateTo("updateDb/account"); }

    void ActiveUserRedirect() { uriHelper.NavigateTo("updateDb/activeUser"); }

    void BikeShopRedirect() { uriHelper.NavigateTo("updateDb/bikeShop"); }

    void DowntimeReportRedirect() { uriHelper.NavigateTo("updateDb/downtimeReport"); }

    void ItemRedirect() { uriHelper.NavigateTo("updateDb/item"); }

    void LogRedirect() { uriHelper.NavigateTo("updateDb/log"); }

    void TrafficRedirect() { uriHelper.NavigateTo("updateDb/traffic"); }

    void TransactionRedirect() { uriHelper.NavigateTo("updateDb/transaction"); }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager uriHelper { get; set; }
    }
}
#pragma warning restore 1591
