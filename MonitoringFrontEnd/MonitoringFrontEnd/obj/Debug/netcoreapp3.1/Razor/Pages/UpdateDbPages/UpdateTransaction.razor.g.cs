#pragma checksum "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "db438663ccbe66908a6e99b96ff5ea379d5bcd7b"
// <auto-generated/>
#pragma warning disable 1591
namespace MonitoringFrontEnd.Pages.UpdateDbPages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using MonitoringFrontEnd;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\_Imports.razor"
using MonitoringFrontEnd.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
using MonitoringFrontEnd.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
using Newtonsoft.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
using System.Text;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
           [Authorize(Roles = "ADMIN")]

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/updateDB/transaction")]
    public partial class UpdateTransaction : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3>Update a Transaction</h3>");
#nullable restore
#line 11 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
 if (LastSubmitResult != null)
{

#line default
#line hidden
#nullable disable
            __builder.OpenElement(1, "h2");
            __builder.AddMarkupContent(2, "\r\n        Last submit status: ");
            __builder.AddContent(3, 
#nullable restore
#line 14 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                             LastSubmitResult

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(4, "\r\n    ");
            __builder.OpenElement(5, "table");
            __builder.AddAttribute(6, "class", "table");
            __builder.AddMarkupContent(7, "<tbody><tr><th>transaction_id</th>\r\n                <th>total_cost</th>\r\n                <th>date</th>\r\n                <th>account_id</th>\r\n                <th>bikeshop_id</th></tr></tbody>\r\n        ");
            __builder.OpenElement(8, "tbody");
            __builder.OpenElement(9, "tr");
            __builder.OpenElement(10, "td");
            __builder.AddContent(11, 
#nullable restore
#line 28 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                     _transaction.transaction_id

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(12, "\r\n                ");
            __builder.OpenElement(13, "td");
            __builder.AddContent(14, 
#nullable restore
#line 29 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                     _transaction.total_cost

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(15, "\r\n                ");
            __builder.OpenElement(16, "td");
            __builder.AddContent(17, 
#nullable restore
#line 30 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                     _transaction.date

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(18, "\r\n                ");
            __builder.OpenElement(19, "td");
            __builder.AddContent(20, 
#nullable restore
#line 31 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                     _transaction.account_id

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(21, "\r\n                ");
            __builder.OpenElement(22, "td");
            __builder.AddContent(23, 
#nullable restore
#line 32 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                     _transaction.bikeshop_id

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 36 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
}

#line default
#line hidden
#nullable disable
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(24);
            __builder.AddAttribute(25, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 38 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                  _transaction

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(26, "OnSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 38 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                                           FormSubmitted

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(27, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.OpenElement(28, "div");
                __builder2.AddAttribute(29, "class", "form-group");
                __builder2.AddMarkupContent(30, "<label for=\"id\">Transaction Id: </label>\r\n        ");
                __Blazor.MonitoringFrontEnd.Pages.UpdateDbPages.UpdateTransaction.TypeInference.CreateInputNumber_0(__builder2, 31, 32, "id", 33, "form-control", 34, 
#nullable restore
#line 41 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                                          _transaction.transaction_id

#line default
#line hidden
#nullable disable
                , 35, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => _transaction.transaction_id = __value, _transaction.transaction_id)), 36, () => _transaction.transaction_id);
                __builder2.CloseElement();
                __builder2.AddMarkupContent(37, "\r\n    ");
                __builder2.OpenElement(38, "div");
                __builder2.AddAttribute(39, "class", "form-group");
                __builder2.AddMarkupContent(40, "<label for=\"totalCost\">Total Cost: </label>\r\n        ");
                __Blazor.MonitoringFrontEnd.Pages.UpdateDbPages.UpdateTransaction.TypeInference.CreateInputNumber_1(__builder2, 41, 42, "totalCost", 43, "form-control", 44, 
#nullable restore
#line 45 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                                                 _transaction.total_cost

#line default
#line hidden
#nullable disable
                , 45, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => _transaction.total_cost = __value, _transaction.total_cost)), 46, () => _transaction.total_cost);
                __builder2.CloseElement();
                __builder2.AddMarkupContent(47, "\r\n    ");
                __builder2.OpenElement(48, "div");
                __builder2.AddAttribute(49, "class", "form-group");
                __builder2.AddMarkupContent(50, "<label for=\"date\">Date: </label>\r\n        ");
                __Blazor.MonitoringFrontEnd.Pages.UpdateDbPages.UpdateTransaction.TypeInference.CreateInputDate_2(__builder2, 51, 52, "date", 53, "form-control", 54, 
#nullable restore
#line 49 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                                          _transaction.date

#line default
#line hidden
#nullable disable
                , 55, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => _transaction.date = __value, _transaction.date)), 56, () => _transaction.date);
                __builder2.CloseElement();
                __builder2.AddMarkupContent(57, "\r\n    ");
                __builder2.OpenElement(58, "div");
                __builder2.AddAttribute(59, "class", "form-group");
                __builder2.AddMarkupContent(60, "<label for=\"account\">Associated Account Id: </label>\r\n        ");
                __Blazor.MonitoringFrontEnd.Pages.UpdateDbPages.UpdateTransaction.TypeInference.CreateInputNumber_3(__builder2, 61, 62, "account", 63, "form-control", 64, 
#nullable restore
#line 53 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                                               _transaction.account_id

#line default
#line hidden
#nullable disable
                , 65, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => _transaction.account_id = __value, _transaction.account_id)), 66, () => _transaction.account_id);
                __builder2.CloseElement();
                __builder2.AddMarkupContent(67, "\r\n    ");
                __builder2.OpenElement(68, "div");
                __builder2.AddAttribute(69, "class", "form-group");
                __builder2.AddMarkupContent(70, "<label for=\"bikeshop\">Associated BikeShop Id: </label>\r\n        ");
                __Blazor.MonitoringFrontEnd.Pages.UpdateDbPages.UpdateTransaction.TypeInference.CreateInputNumber_4(__builder2, 71, 72, "bikeshop", 73, "form-control", 74, 
#nullable restore
#line 57 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                                                _transaction.bikeshop_id

#line default
#line hidden
#nullable disable
                , 75, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => _transaction.bikeshop_id = __value, _transaction.bikeshop_id)), 76, () => _transaction.bikeshop_id);
                __builder2.CloseElement();
                __builder2.AddMarkupContent(77, "\r\n    <input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">\r\n    ");
                __builder2.OpenElement(78, "input");
                __builder2.AddAttribute(79, "type", "button");
                __builder2.AddAttribute(80, "class", "btn btn-secondary");
                __builder2.AddAttribute(81, "value", "Cancel");
                __builder2.AddAttribute(82, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 60 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
                                                                            onCancelButton

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseElement();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 63 "C:\Users\Jonathan\source\repos\MonitoringBikeShop\MonitoringBikeShop\MonitoringFrontEnd\MonitoringFrontEnd\Pages\UpdateDbPages\UpdateTransaction.razor"
       
    int index = 1;
    string strUri;
    Transaction _transaction = new Transaction();

    string LastSubmitResult;

    async Task FormSubmitted(EditContext editContext)
    {
        bool isFormValid = editContext.Validate();
        if (isFormValid)
        {
            index = _transaction.transaction_id;
            if (index > 0)
            {
                //create Json object
                string jsonString = JsonConvert.SerializeObject(_transaction,
                    Formatting.None,
                    new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });


                //create content to send to api
                HttpContent postContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
                strUri = "https://theheartofthecardz.azurewebsites.net/api/Transactions/" + index;

                var response = await api.PutUriAsync(new Uri(strUri), postContent);
                var result = response.Content.ReadAsStringAsync();
                Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(result.Result.ToString());
                _transaction = json.ToObject<Transaction>();

                LastSubmitResult = response.StatusCode.ToString();
            }
            else
                LastSubmitResult = "Failure: Form was invalid";

            return;
        }
    }


    void onCancelButton()
    {
        uriHelper.NavigateTo("updateDb");
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager uriHelper { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Services.ApiService api { get; set; }
    }
}
namespace __Blazor.MonitoringFrontEnd.Pages.UpdateDbPages.UpdateTransaction
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateInputNumber_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, System.Object __arg1, int __seq2, TValue __arg2, int __seq3, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg4)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputNumber<TValue>>(seq);
        __builder.AddAttribute(__seq0, "id", __arg0);
        __builder.AddAttribute(__seq1, "class", __arg1);
        __builder.AddAttribute(__seq2, "Value", __arg2);
        __builder.AddAttribute(__seq3, "ValueChanged", __arg3);
        __builder.AddAttribute(__seq4, "ValueExpression", __arg4);
        __builder.CloseComponent();
        }
        public static void CreateInputNumber_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, System.Object __arg1, int __seq2, TValue __arg2, int __seq3, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg4)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputNumber<TValue>>(seq);
        __builder.AddAttribute(__seq0, "id", __arg0);
        __builder.AddAttribute(__seq1, "class", __arg1);
        __builder.AddAttribute(__seq2, "Value", __arg2);
        __builder.AddAttribute(__seq3, "ValueChanged", __arg3);
        __builder.AddAttribute(__seq4, "ValueExpression", __arg4);
        __builder.CloseComponent();
        }
        public static void CreateInputDate_2<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, System.Object __arg1, int __seq2, TValue __arg2, int __seq3, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg4)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputDate<TValue>>(seq);
        __builder.AddAttribute(__seq0, "id", __arg0);
        __builder.AddAttribute(__seq1, "class", __arg1);
        __builder.AddAttribute(__seq2, "Value", __arg2);
        __builder.AddAttribute(__seq3, "ValueChanged", __arg3);
        __builder.AddAttribute(__seq4, "ValueExpression", __arg4);
        __builder.CloseComponent();
        }
        public static void CreateInputNumber_3<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, System.Object __arg1, int __seq2, TValue __arg2, int __seq3, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg4)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputNumber<TValue>>(seq);
        __builder.AddAttribute(__seq0, "id", __arg0);
        __builder.AddAttribute(__seq1, "class", __arg1);
        __builder.AddAttribute(__seq2, "Value", __arg2);
        __builder.AddAttribute(__seq3, "ValueChanged", __arg3);
        __builder.AddAttribute(__seq4, "ValueExpression", __arg4);
        __builder.CloseComponent();
        }
        public static void CreateInputNumber_4<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, System.Object __arg1, int __seq2, TValue __arg2, int __seq3, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg4)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputNumber<TValue>>(seq);
        __builder.AddAttribute(__seq0, "id", __arg0);
        __builder.AddAttribute(__seq1, "class", __arg1);
        __builder.AddAttribute(__seq2, "Value", __arg2);
        __builder.AddAttribute(__seq3, "ValueChanged", __arg3);
        __builder.AddAttribute(__seq4, "ValueExpression", __arg4);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
