﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MonitoringFrontEnd.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MonitoringFrontEnd
{
    //Adapted from https://nbarbettini.gitbooks.io/little-asp-net-core-book/content/chapters/security-and-identity/authorization-with-roles.html
    public static class SeedData
    {
        public static async Task InitializeAsync(IServiceProvider services)
        {
            var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
            await EnsureRolesAsync(roleManager);

            var userManager = services.GetRequiredService<UserManager<IdentityUser>>();
            await EnsureTestAdminAsync(userManager);
        }


        private static async Task EnsureRolesAsync(RoleManager<IdentityRole> roleManager)
        {
            var alreadyExistsAdmin = await roleManager.RoleExistsAsync("ADMIN");

            if (alreadyExistsAdmin)
            {
                var alreadyExistsNormal = await roleManager.RoleExistsAsync("NORMAL");

                if (alreadyExistsNormal) return;

                await roleManager.CreateAsync(new IdentityRole{
                    Id = "normal",
                    Name = "NORMAL",
                    NormalizedName = "NORMAL" });

                return;
            }

            await roleManager.CreateAsync(new IdentityRole {
                Id = "admin",
                Name = "ADMIN",
                NormalizedName = "ADMIN"});
        }


        private static async Task EnsureTestAdminAsync(UserManager<IdentityUser> userManager)
        {
            var testAdmin = await userManager.Users.Where(x => x.UserName == "admin@todo.local").SingleOrDefaultAsync();

            if (testAdmin != null) return;

            testAdmin = new IdentityUser
            {
                UserName = "admin@todo.local",
                Email = "admin@todo.local"
            };

            await userManager.CreateAsync(testAdmin, "NotSecure123!!");
            await userManager.AddToRoleAsync(testAdmin, "ADMIN");
        }
    }
}
