namespace MonitoringFrontEnd.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Log report sent by BikeShop website
    /// </summary>
    public partial class Log
    {
        public Log()
        {

        }

        /// <summary>
        /// Log report's id
        /// </summary>
        [JsonProperty(PropertyName = "log_id")]
        public int log_id { get; set; }
        /// <summary>
        /// Application for the log
        /// </summary>
        [JsonProperty(PropertyName = "application")]
        public string application { get; set; }
        /// <summary>
        /// Date logged
        /// </summary>
        [JsonProperty(PropertyName = "logged")]
        public Nullable<System.DateTime> logged { get; set; }
        /// <summary>
        /// Logged level
        /// </summary>
        [JsonProperty(PropertyName = "level")]
        public string level { get; set; }
        /// <summary>
        /// Message with log
        /// </summary>
        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }
        /// <summary>
        /// Logger used
        /// </summary>
        [JsonProperty(PropertyName = "logger")]
        public string logger { get; set; }
        /// <summary>
        /// Callsite used
        /// </summary>
        [JsonProperty(PropertyName = "callsite")]
        public string callsite { get; set; }
        /// <summary>
        /// Exceptions given
        /// </summary>
        [JsonProperty(PropertyName = "exception")]
        public string exception { get; set; }
    }
}
